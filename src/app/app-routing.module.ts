import {NgModule} from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';
import {RoutesConst} from './utils/constants/routes.const';

const routes: Routes = [
  {path: RoutesConst.clean, redirectTo: RoutesConst.pages.path, pathMatch: 'full'},
  {
    path: RoutesConst.pages.path,
    loadChildren: './pages/pages.module#PagesModule'
  },
  {path: '**', redirectTo: RoutesConst.pages.notFound.fullPath}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true, preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
