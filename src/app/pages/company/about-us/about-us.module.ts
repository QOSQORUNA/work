import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutUsComponent } from './about-us.component';
import {MatIconModule} from '@angular/material/icon';
import {AboutUsRoutingModule} from './about-us-routing.module';

@NgModule({
  declarations: [AboutUsComponent],
  imports: [
    CommonModule,
    MatIconModule,
    AboutUsRoutingModule
  ]
})
export class AboutUsModule { }
