import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {RoutesConst} from '../../../utils/constants/routes.const';
import {AboutUsComponent} from './about-us.component';

export const routes: Routes = [
  {
    path: RoutesConst.clean,
    component: AboutUsComponent,
    data: {breadcrumb: 'Nosotros'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AboutUsRoutingModule {
}
