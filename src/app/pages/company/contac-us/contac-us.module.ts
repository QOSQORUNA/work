import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContacUsComponent } from './contac-us.component';
import {ContactUsRoutingModule} from './contact-us-routing.module';

@NgModule({
  declarations: [ContacUsComponent],
  imports: [
    CommonModule,
    ContactUsRoutingModule
  ]
})
export class ContacUsModule { }
