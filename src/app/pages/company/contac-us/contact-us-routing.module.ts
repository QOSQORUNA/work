import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {RoutesConst} from '../../../utils/constants/routes.const';
import {ContacUsComponent} from './contac-us.component';


export const routes: Routes = [
  {
    path: RoutesConst.clean,
    component: ContacUsComponent,
    data: {breadcrumb: 'Contacto'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactUsRoutingModule {
}
