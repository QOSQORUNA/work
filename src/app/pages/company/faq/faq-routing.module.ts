import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {RoutesConst} from '../../../utils/constants/routes.const';
import {FaqComponent} from './faq.component';


export const routes: Routes = [
  {
    path: RoutesConst.clean,
    component: FaqComponent,
    data: {breadcrumb: 'Faq'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FaqRoutingModule {
}
