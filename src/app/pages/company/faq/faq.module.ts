import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqComponent } from './faq.component';
import {FaqRoutingModule} from './faq-routing.module';
import {
  MatButtonModule,
  MatExpansionModule,
  MatIconModule,
} from '@angular/material';

@NgModule({
  declarations: [FaqComponent],
  imports: [
    CommonModule,
    FaqRoutingModule,
    MatIconModule,
    MatButtonModule,
    MatExpansionModule
  ]
})
export class FaqModule { }
