import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {RoutesConst} from '../utils/constants/routes.const';
import {PagesComponent} from './pages.component';
import {UnauthorizedComponent} from '../components/error-pages/unauthorized/unauthorized.component';
import {ServerErrorComponent} from '../components/error-pages/server-error/server-error.component';
import {NotFoundComponent} from '../components/error-pages/not-found/not-found.component';

export const routes: Routes = [
  {
    path: RoutesConst.clean,
    component: PagesComponent,
    children: [
      {path: RoutesConst.clean, redirectTo: RoutesConst.pages.home.path, pathMatch: 'full'},
      {
        path: RoutesConst.pages.home.path,
        loadChildren: './home/home.module#HomeModule',
        data: {title: RoutesConst.pages.home.title, subtitle: RoutesConst.pages.home.subtitle},
      },
      {
        path: RoutesConst.pages.about_us.path,
        loadChildren: './company/about-us/about-us.module#AboutUsModule',
        data: {title: RoutesConst.pages.about_us.title, subtitle: RoutesConst.pages.about_us.subtitle},
      },
      {
        path: RoutesConst.pages.contact_us.path,
        loadChildren: './company/contac-us/contac-us.module#ContacUsModule',
        data: {title: RoutesConst.pages.contact_us.title, subtitle: RoutesConst.pages.contact_us.subtitle},
      },
      {
        path: RoutesConst.pages.faq.path,
        loadChildren: './company/faq/faq.module#FaqModule',
        data: {title: RoutesConst.pages.faq.title, subtitle: RoutesConst.pages.faq.subtitle},
      },
      {
        path: RoutesConst.pages.cemetery.path,
        loadChildren: './cemetery/cemetery.module#CemeteryModule',
        data: {title: RoutesConst.pages.cemetery.title, subtitle: RoutesConst.pages.cemetery.subtitle},
      },
      {
        path: RoutesConst.pages.unauthorized.path,
        component: UnauthorizedComponent,
        data: {title: RoutesConst.pages.unauthorized.title, subtitle: RoutesConst.pages.unauthorized.subtitle}
      },
      {
        path: RoutesConst.pages.serverError.path,
        component: ServerErrorComponent,
        data: {title: RoutesConst.pages.serverError.title, subtitle: RoutesConst.pages.serverError.subtitle}
      },
      {
        path: RoutesConst.pages.notFound.path,
        component: NotFoundComponent,
        data: {title: RoutesConst.pages.notFound.title, subtitle: RoutesConst.pages.notFound.subtitle}
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {
}
