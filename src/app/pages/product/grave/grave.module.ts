import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GraveComponent } from './grave.component';

@NgModule({
  declarations: [GraveComponent],
  imports: [
    CommonModule
  ]
})
export class GraveModule { }
