import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CremationComponent } from './cremation.component';

describe('CremationComponent', () => {
  let component: CremationComponent;
  let fixture: ComponentFixture<CremationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CremationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CremationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
