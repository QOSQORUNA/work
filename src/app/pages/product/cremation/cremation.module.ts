import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CremationComponent } from './cremation.component';

@NgModule({
  declarations: [CremationComponent],
  imports: [
    CommonModule
  ]
})
export class CremationModule { }
