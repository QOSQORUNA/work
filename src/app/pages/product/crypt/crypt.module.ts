import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CryptComponent } from './crypt.component';

@NgModule({
  declarations: [CryptComponent],
  imports: [
    CommonModule
  ]
})
export class CryptModule { }
