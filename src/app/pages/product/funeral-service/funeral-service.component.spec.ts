import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuneralServiceComponent } from './funeral-service.component';

describe('FuneralServiceComponent', () => {
  let component: FuneralServiceComponent;
  let fixture: ComponentFixture<FuneralServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuneralServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuneralServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
