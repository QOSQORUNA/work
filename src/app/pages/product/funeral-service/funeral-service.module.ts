import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FuneralServiceComponent } from './funeral-service.component';

@NgModule({
  declarations: [FuneralServiceComponent],
  imports: [
    CommonModule
  ]
})
export class FuneralServiceModule { }
