import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {RoutesConst} from '../../utils/constants/routes.const';
import {CemeteryComponent} from './cemetery.component';

export const routes: Routes = [
  {
    path: RoutesConst.clean,
    component: CemeteryComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CemeteryRoutingModule {
}
