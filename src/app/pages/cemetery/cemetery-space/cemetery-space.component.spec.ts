import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CemeterySpaceComponent } from './cemetery-space.component';

describe('CemeterySpaceComponent', () => {
  let component: CemeterySpaceComponent;
  let fixture: ComponentFixture<CemeterySpaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CemeterySpaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CemeterySpaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
