import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CemeteryInfoComponent } from './cemetery-info.component';

describe('CemeteryInfoComponent', () => {
  let component: CemeteryInfoComponent;
  let fixture: ComponentFixture<CemeteryInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CemeteryInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CemeteryInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
