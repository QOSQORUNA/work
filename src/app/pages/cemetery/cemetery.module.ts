import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CemeteryComponent} from './cemetery.component';
import {CemeteryInfoComponent} from './cemetery-info/cemetery-info.component';
import {CemeterySpaceComponent} from './cemetery-space/cemetery-space.component';
import {CemeteryRoutingModule} from './cemetery-routing.module';
import {MatTabsModule} from '@angular/material/tabs';

@NgModule({
  declarations: [CemeteryComponent, CemeteryInfoComponent, CemeterySpaceComponent],
  imports: [
    CommonModule,
    CemeteryRoutingModule,
    MatTabsModule
  ]
})
export class CemeteryModule {
}
