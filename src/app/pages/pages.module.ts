import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PagesComponent} from './pages.component';
import {PagesRoutingModule} from './pages-routing.module';
import {LayoutsModule} from '../components/layouts/layouts.module';
import {NotFoundComponent} from '../components/error-pages/not-found/not-found.component';
import {ServerErrorComponent} from '../components/error-pages/server-error/server-error.component';
import {UnauthorizedComponent} from '../components/error-pages/unauthorized/unauthorized.component';
import {MatIconModule} from '@angular/material';

@NgModule({
  declarations: [
    PagesComponent,
    NotFoundComponent,
    ServerErrorComponent,
    UnauthorizedComponent
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    LayoutsModule,
    MatIconModule
  ]
})
export class PagesModule {
}
