import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExternalService {

  constructor() {
  }

  /**
   * Guardar información de contacto en referidos
   */
  referredRegister(): Observable<any> {
    // ?appCode={appCode} debe estar en enviroments
    return of({});
  }
}
