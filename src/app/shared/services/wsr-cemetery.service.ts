import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WsrCemeteryService {

  constructor() {
  }

  /**
   * Obtener lista de camposantos
   */
  getCemeteries(): Observable<any> {
    return of({});
  }

  /**
   * Obtener espacios de un camposanto
   * @param cemeteryId id del camposanto
   */
  getSpaces(cemeteryId: any): Observable<any> {
    return of({});
  }

  /**
   * Obtener imagenes de un camposanto
   * @param cemeteryId id del camposanto
   */
  getPictures(cemeteryId: any): Observable<any> {
    return of({});
  }

  /**
   * Obtener data de la web camposanto
   */
  getPortalData(): Observable<any> {
    // ?appCode={appCode} debe estar en enviroments
    return of({});
  }

}
