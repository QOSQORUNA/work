import {Injectable} from '@angular/core';
import {Cemetery} from '../models/internal/cemetery';
import {Parameter} from '../models/internal/parameter';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private _cemeteries: Array<Cemetery> = [];
  private _parameters: Array<Parameter> = [];

  private _isDesktop = false;
  private _isTablet = false;
  private _isMobile = false;

  constructor() {
  }

  //#region SETTERS AND GETTERS

  get cemeteries(): Array<Cemetery> {
    return this._cemeteries;
  }

  set cemeteries(value: Array<Cemetery>) {
    this._cemeteries = value;
  }

  get parameters(): Array<Parameter> {
    return this._parameters;
  }

  set parameters(value: Array<Parameter>) {
    this._parameters = value;
  }

  get isDesktop(): boolean {
    return this._isDesktop;
  }

  set isDesktop(value: boolean) {
    this._isDesktop = value;
  }

  get isTablet(): boolean {
    return this._isTablet;
  }

  set isTablet(value: boolean) {
    this._isTablet = value;
  }

  get isMobile(): boolean {
    return this._isMobile;
  }

  set isMobile(value: boolean) {
    this._isMobile = value;
  }
//#endregion
}
