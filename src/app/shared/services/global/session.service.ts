import {Injectable} from '@angular/core';
import {HttpHeaders} from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class SessionService {

  public static OBJ_LOGIN = 'token';

  constructor() {
  }

  exists(key) {
    return localStorage.getItem(key);
  }

  setItem(key, value) {
    localStorage.setItem(key, value);
  }

  public getItem(key): any {
    return localStorage.getItem(key);
  }

  setObject(key, object) {
    const value = JSON.stringify(object);
    localStorage.setItem(key, value);
  }

  getObject(key) {
    const value = localStorage.getItem(key);
    return (value && value !== 'undefined' && value !== 'null' ? JSON.parse(value) : null);
  }

  clear() {
    localStorage.clear();
  }

  getHeader(_header: number = null): HttpHeaders {
    if (_header === 1) {
      const oaut = this.getObject(SessionService.OBJ_LOGIN);
      return new HttpHeaders({
        Authorization: 'bearer ' + (oaut ? oaut.accessToken : '')
      });
    }
    return new HttpHeaders({});
  }

  getHeaderMultipart(_header: number = null): HttpHeaders {
    if (_header === 1) {
      const oaut = this.getObject(SessionService.OBJ_LOGIN);
      return new HttpHeaders({
        Authorization: 'bearer ' + (oaut ? oaut.token : '')
      });
    }
    return new HttpHeaders({
      // headers.append('Content-Type', 'multipart/form-data');
    });
  }
}
