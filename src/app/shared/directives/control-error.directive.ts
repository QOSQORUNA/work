import {AfterViewInit, Directive, ElementRef, Input, Renderer2} from '@angular/core';

@Directive({
  selector: '[mfControlError]'
})
export class ControlErrorDirective implements AfterViewInit {
  private FORMAT = 'DD/MM/YYYY';
  private _mpfDirty = false;
  private _mpfErrors: any = null;

  @Input() msgRequired = 'El campo es obligatorio';
  @Input() msgMinlength = 'Ingrese mínimo {requiredLength} caracteres';
  @Input() msgMaxlength = 'Ingrese máximo {requiredLength} caracteres';
  @Input() msgPattern = 'El formato del valor es inválido';
  @Input() msgInvalidDate = 'La fecha es inválida';
  @Input() msgMinDate = 'La fecha mínima es {minDate}';
  @Input() msgMaxDate = 'La fecha máxima es {maxDate}';

  @Input()
  set mpfDirty(value: boolean) {
    this._mpfDirty = value;
    this.loadError();
  }

  get mpfDirty(): boolean {
    return this._mpfDirty;
  }

  @Input()
  set mpfErrors(value: any) {
    this._mpfErrors = value;
    this.loadError();
  }

  get mpfErrors(): any {
    return this._mpfErrors;
  }

  constructor(private readonly _elementRef: ElementRef,
              private readonly render2: Renderer2) {
  }

  ngAfterViewInit() {
    this.removeHtml();
  }

  private loadError() {
    if (this.mpfDirty && this.mpfErrors) {
      this.removeHtml();
      if (this.mpfErrors.required && !this.mpfErrors.matDatepickerParse) {
        this.createHtml(this.msgRequired);
      } else if (this.mpfErrors.minlength) {
        this.createHtml(this.msgMinlength.replace('{requiredLength}', this.mpfErrors.minlength.requiredLength));
      } else if (this.mpfErrors.maxlength && !this.mpfErrors.minlength) {
        this.createHtml(this.msgMaxlength.replace('{requiredLength}', this.mpfErrors.maxlength.requiredLength));
      } else if (this.mpfErrors.pattern && !this.mpfErrors.maxlength && !this.mpfErrors.minlength) {
        this.createHtml(this.msgPattern);
      } else if (this.mpfErrors.matDatepickerParse && this.mpfErrors.matDatepickerParse.text) {
        this.createHtml(this.msgInvalidDate);
      } else if (this.mpfErrors.matDatepickerMin && this.mpfErrors.matDatepickerMin.min) {
        const minDate = this.mpfErrors.matDatepickerMin.min.format(this.FORMAT);
        this.createHtml(this.msgMinDate.replace('{minDate}', minDate));
      } else if (this.mpfErrors.matDatepickerMax && this.mpfErrors.matDatepickerMax.max) {
        const maxDate = this.mpfErrors.matDatepickerMax.max.format(this.FORMAT);
        this.createHtml(this.msgMaxDate.replace('{maxDate}', maxDate));
      }
    }
  }

  private removeHtml() {
    if (this._elementRef.nativeElement.firstChild) {
      this._elementRef.nativeElement.removeChild(this._elementRef.nativeElement.firstChild);
    }
  }

  private createHtml(message: string) {
    this.render2.setProperty(this._elementRef.nativeElement, 'innerHTML', message);
  }
}
