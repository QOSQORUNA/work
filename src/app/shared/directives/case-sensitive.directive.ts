import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[mfCaseSensitive]'
})
export class CaseSensitiveDirective {
  @Input('caseType') caseType = '';

  constructor(private _el: ElementRef) {
  }

  @HostListener('input', ['$event']) onKeyUp(event) {
    const value = event.target.value;
    if (this.caseType === 'upper') {
      event.target.value = value.toUpperCase();
    } else if (this.caseType === 'lower') {
      event.target.value = value.toLowerCase();
    }
  }
}
