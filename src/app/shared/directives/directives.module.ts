import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OnlyInputDirective} from './only-input.directive';
import {CaseSensitiveDirective} from './case-sensitive.directive';
import {ControlErrorDirective} from './control-error.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    OnlyInputDirective,
    CaseSensitiveDirective,
    ControlErrorDirective,
  ],
  exports: [
    OnlyInputDirective,
    CaseSensitiveDirective,
    ControlErrorDirective,
  ]
})
export class DirectivesModule {
}
