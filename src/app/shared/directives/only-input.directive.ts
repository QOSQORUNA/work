import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[mfOnlyInput]'
})
export class OnlyInputDirective {
  @Input('onlyType') onlyType: string;

  private regex = {
    number: new RegExp(/^[0-9]\d*$/),
    decimal: new RegExp(/^[0-9]+(\.[0-9]*){0,1}$/),
    percent: new RegExp(/^[1-9][0-9]*$/),
    alphabetical: /^[a-zA-Z\u00C0-\u00FF]+$/,
    alphabeticals: /^[a-zA-Z\u00C0-\u00FF ]+$/,
    alphanumeric: new RegExp(/^[0-9a-zA-Z\u00C0-\u00FF]+$/),
    alphanumerics: new RegExp(/^[0-9a-zA-Z\u00C0-\u00FF ]+$/),
    cell: new RegExp(/^9[0-9]*$/),
    ruc10: new RegExp(/^1[0-9]*$/),
    noSpaces: new RegExp(/^\S*$/gmi)
  };

  private specialKeys = {
    number: ['Backspace', 'Tab', 'Enter', 'Escape', 'End', 'Home', 'ArrowLeft', 'ArrowRight', 'Delete'],
    decimal: ['Backspace', 'Tab', 'Enter', 'Escape', 'End', 'Home', 'ArrowLeft', 'ArrowRight', 'Delete'],
    percent: ['Backspace', 'Tab', 'Enter', 'Escape', 'End', 'Home', 'ArrowLeft', 'ArrowRight', 'Delete'],
    alphabetical: ['Backspace', 'Tab', 'Enter', 'Escape', 'End', 'Home', 'ArrowLeft', 'ArrowRight', 'Delete'],
    alphabeticals: ['Backspace', 'Tab', 'Enter', 'Escape', 'End', 'Home', 'ArrowLeft', 'ArrowRight', 'Delete'],
    alphanumeric: ['Backspace', 'Tab', 'Enter', 'Escape', 'End', 'Home', 'ArrowLeft', 'ArrowRight', 'Delete'],
    alphanumerics: ['Backspace', 'Tab', 'Enter', 'Escape', 'End', 'Home', 'ArrowLeft', 'ArrowRight', 'Delete'],
    cell: ['Backspace', 'Tab', 'Enter', 'Escape', 'End', 'Home', 'ArrowLeft', 'ArrowRight', 'Delete'],
    ruc10: ['Backspace', 'Tab', 'Enter', 'Escape', 'End', 'Home', 'ArrowLeft', 'ArrowRight', 'Delete'],
    noSpaces: ['Backspace', 'Tab', 'Enter', 'Escape', 'End', 'Home', 'ArrowLeft', 'ArrowRight', 'Delete']
  };

  constructor(private el: ElementRef) {
  }

  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (!this.onlyType) {
      return;
    }
    if (this.specialKeys[this.onlyType].indexOf(event.key) !== -1
      || (event.keyCode === 65 && event.ctrlKey) // Allow: Ctrl+A
      || (event.keyCode === 67 && event.ctrlKey) // Allow: Ctrl+C
      || (event.keyCode === 86 && event.ctrlKey) // Allow: Ctrl+V
      || (event.keyCode === 88 && event.ctrlKey) // Allow: Ctrl+X
    ) {
      return;
    }

    const current: string = this.el.nativeElement.value;
    const next: string = current.concat(event.key);
    if (next && !String(next).match(this.regex[this.onlyType])) {
      event.preventDefault();
    }
  }
}
