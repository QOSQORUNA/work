import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {Router} from '@angular/router';
import {catchError} from 'rxjs/internal/operators';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {RoutesConst} from '../../utils/constants/routes.const';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  private routes = RoutesConst.pages;
  private urlExcludes: Array<string> = [];

  constructor(private readonly router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse) => {
        if (err.status === 401 && !this.inUrlExclude(request.url)) {
          this.router.navigate([this.routes.unauthorized.fullPath]);

        } else if (!this.inUrlExclude(request.url)) {
          this.router.navigate([this.routes.serverError.fullPath]);
        }

        return throwError(err);
      })
    );
  }

  private inUrlExclude(urlRequest: string): boolean {
    let resp = false;
    for (const url of this.urlExcludes) {
      if (urlRequest.search(url) !== -1) {
        resp = true;
        break;
      }
    }
    return resp;
  }
}
