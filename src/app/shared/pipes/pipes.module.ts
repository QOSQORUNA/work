import {NgModule} from '@angular/core';
import {CommonModule, DecimalPipe} from '@angular/common';
import {FormatDatePipe} from './format-date.pipe';
import {MoneyPipe} from './money.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    FormatDatePipe,
    MoneyPipe
  ],
  exports: [
    FormatDatePipe,
    MoneyPipe
  ],
  providers: [
    DecimalPipe
  ]
})
export class PipesModule {
}
