import {Pipe, PipeTransform} from '@angular/core';
import {DecimalPipe} from '@angular/common';
import {isNumeric} from 'rxjs/internal/util/isNumeric';

@Pipe({
  name: 'money'
})
export class MoneyPipe implements PipeTransform {
  private _currency = 'S/';
  private _decimal = 2;

  constructor(private decimalPipe?: DecimalPipe) {
  }

  transform(value: number | string, params?: { currency?: string, decimal?: number }): string {
    if (params) {
      if (!params.currency) {
        params.currency = this._currency;
      }
      if (params.decimal === undefined || params.decimal === null) {
        params.decimal = this._decimal;
      }
    } else {
      params = {currency: this._currency, decimal: this._decimal};
    }

    if (value && isNumeric(value)) {
      return params.currency + ' ' + this.decimalPipe.transform(Number(value), ('.' + params.decimal + '-' + params.decimal));
    }
    return this._currency + ' ' + this.decimalPipe.transform(0, ('.' + params.decimal + '-' + params.decimal));
  }
}
