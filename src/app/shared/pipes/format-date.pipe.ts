import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'formatDate'
})
export class FormatDatePipe implements PipeTransform {

  transform(value: any, format?: string): string {
    format = format ? format : 'DD/MM/YYYY';
    if (value && moment(value).isValid()) {
      return moment(value).format(format);
    }
    return '';
  }
}
