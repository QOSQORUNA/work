import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {RoutesConst} from '../../../utils/constants/routes.const';

@Component({
  selector: 'mf-unauthorized',
  templateUrl: './unauthorized.component.html',
  styleUrls: ['./unauthorized.component.scss']
})
export class UnauthorizedComponent implements OnInit {
  routes = RoutesConst.pages;

  constructor(private readonly router: Router) {
  }

  ngOnInit() {
  }

  onHome() {
    this.router.navigate([this.routes.home.fullPath]);
  }
}
