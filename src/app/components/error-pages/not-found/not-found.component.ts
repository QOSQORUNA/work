import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {RoutesConst} from '../../../utils/constants/routes.const';

@Component({
  selector: 'mf-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {
  routes = RoutesConst.pages;

  constructor(private readonly router: Router) {
  }

  ngOnInit() {
  }

  onHome() {
    this.router.navigate([this.routes.home.fullPath]);
  }
}
