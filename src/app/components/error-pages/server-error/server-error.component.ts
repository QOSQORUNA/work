import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {RoutesConst} from '../../../utils/constants/routes.const';

@Component({
  selector: 'mf-server-error',
  templateUrl: './server-error.component.html',
  styleUrls: ['./server-error.component.scss']
})
export class ServerErrorComponent implements OnInit {
  routes = RoutesConst.pages;

  constructor(private location: Location) {
  }

  ngOnInit() {
  }

  onBack() {
    this.location.back();
  }
}
