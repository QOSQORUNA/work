import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'mf-notice-modal',
  templateUrl: './notice-modal.component.html',
  styleUrls: ['./notice-modal.component.scss']
})
export class NoticeModalComponent implements OnInit {

  constructor(private readonly dialogRef: MatDialogRef<NoticeModalComponent>) { }

  ngOnInit() {
  }

  onClose(): void {
    this.dialogRef.close(true);
  }
}
