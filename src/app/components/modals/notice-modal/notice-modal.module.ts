import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoticeModalComponent } from './notice-modal.component';
import {MatButtonModule, MatDialogModule, MatIconModule} from '@angular/material';

@NgModule({
  declarations: [NoticeModalComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule
  ],
  exports: [
    NoticeModalComponent
  ],
  entryComponents: [
    NoticeModalComponent
  ]
})
export class NoticeModalModule { }
