import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {NoticeModalModule} from '../modals/notice-modal/notice-modal.module';
import {MatDialogModule} from '@angular/material';
import {NoticeModalComponent} from '../modals/notice-modal/notice-modal.component';

@NgModule({
  declarations: [HeaderComponent, FooterComponent],
  exports: [
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    NoticeModalModule,
    MatDialogModule
  ],
  entryComponents:[
    NoticeModalComponent
  ]
})
export class LayoutsModule {
}
