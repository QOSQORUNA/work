import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {DataService} from '../../../shared/services/data.service';
import {WsrCemeteryService} from '../../../shared/services/wsr-cemetery.service';
import {RoutesConst} from '../../../utils/constants/routes.const';
import {MatDialog} from '@angular/material';
import {NoticeModalComponent} from '../../modals/notice-modal/notice-modal.component';

@Component({
  selector: 'mf-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @ViewChild('header') headerElemRef: ElementRef;

  constructor(private readonly router: Router,
              public readonly dataService: DataService,
              private readonly wsrCemeteryService: WsrCemeteryService,
              private readonly dialog: MatDialog) { }

  ngOnInit() {
  }


  openNoticeModal(): void {
    this.dialog.open(NoticeModalComponent, {
      width: '1000px', autoFocus: false, disableClose: true
    });
  }
  onHome() {
    this.router.navigate([RoutesConst.pages.home.fullPath]);
  }
}
