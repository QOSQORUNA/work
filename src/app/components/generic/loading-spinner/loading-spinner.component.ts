import {Component, Input, OnDestroy, ViewEncapsulation} from '@angular/core';
import {Subscription} from 'rxjs';
import {LoadingSpinnerService} from './loading-spinner.service';

@Component({
  selector: 'mf-loading-spinner',
  templateUrl: './loading-spinner.component.html',
  styleUrls: ['./loading-spinner.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoadingSpinnerComponent implements OnDestroy {
  _template = `<div class='lds-roller'><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>`;
  _loadingText = 'hola que tal ';
  _threshold = 500;
  _timeout = 0;
  _zIndex = 9999;

  @Input()
  public set zIndex(value: number) {
    this._zIndex = value;
  }

  public get zIndex(): number {
    return this._zIndex;
  }

  @Input()
  public set template(value: string) {
    this._template = value;
  }

  public get template(): string {
    return this._template;
  }

  @Input()
  public set loadingText(value: string) {
    this._loadingText = value;
  }

  public get loadingText(): string {
    return this._loadingText;
  }

  @Input()
  public set threshold(value: number) {
    this._threshold = value;
  }

  public get threshold(): number {
    return this._threshold;
  }

  @Input()
  public set timeout(value: number) {
    this._timeout = value;
  }

  public get timeout(): number {
    return this._timeout;
  }

  subscription: Subscription;
  showSpinner = false;

  public lottieConfig: any;
  private anim: any;

  constructor(private spinnerService: LoadingSpinnerService) {
    this.lottieConfig = {
      path: 'assets/images/animations/loading.json',
      autoplay: true,
      loop: true
    };
    this.createServiceSubscription();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  handleAnimation(anim: any) {
    this.anim = anim;
  }
  createServiceSubscription() {
    let thresholdTimer: any;
    let timeoutTimer: any;

    this.subscription = this.spinnerService.getMessage().subscribe(show => {
      if (show) {
        if (thresholdTimer) {
          return;
        }
        thresholdTimer = setTimeout(function () {
          thresholdTimer = null;
          this.showSpinner = show;
          if (this.timeout !== 0) {
            timeoutTimer = setTimeout(function () {
              timeoutTimer = null;
              this.showSpinner = false;
            }.bind(this), this.timeout);
          }
        }.bind(this), this.threshold);
      } else {
        if (thresholdTimer) {
          clearTimeout(thresholdTimer);
          thresholdTimer = null;
        }
        if (timeoutTimer) {
          clearTimeout(timeoutTimer);
        }
        timeoutTimer = null;
        this.showSpinner = false;
      }
    });
  }
}
