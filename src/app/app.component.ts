import {Component, OnDestroy, OnInit} from '@angular/core';
import {ResponsiveService} from './shared/services/global/responsive.service';
import {DataService} from './shared/services/data.service';

@Component({
  selector: 'mf-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'mapfre-doc';
  subscription;
  constructor(private readonly responsiveService: ResponsiveService,
              private readonly dataService: DataService) {
  }
  ngOnInit(): void {
    this.getResposive();
  }

  getResposive() {
    this.subscription = this.responsiveService.windowSizeChanged.subscribe(value => {
      this.dataService.isDesktop = false;
      this.dataService.isTablet = false;
      this.dataService.isMobile = false;

      if (value.width < 768) {
        this.dataService.isMobile = true;
      } else if (value.width < 992) {
        this.dataService.isTablet = true;
      } else {
        this.dataService.isDesktop = true;
      }
    });
  }
}
