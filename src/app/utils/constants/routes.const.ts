export class RoutesConst {
  public static clean = '';
  public static pages = {
    path: 'web',
    home: {
      path: 'inicio',
      fullPath: '/web/inicio',
      title: 'Inicio',
      subtitle: 'Inicio'
    },
    about_us: {
      path: 'nosotros',
      fullPath: '/web/nosotros',
      title: 'Nosotros',
      subtitle: 'Nosotros'
    },
    contact_us: {
      path: 'contacto',
      fullPath: '/web/contacto',
      title: 'Contacto',
      subtitle: 'Contacto'
    },
    faq: {
      path: 'faq',
      fullPath: '/web/faq',
      title: 'Faq',
      subtitle: 'Faq'
    },
    cemetery: {
      path: 'camposanto',
      fullPath: '/web/camposanto',
      title: 'Camposanto',
      subtitle: '{cemeteryName}'
    },
    unauthorized: {
      path: 'no-autorizado',
      fullPath: '/web/no-autorizado',
      title: 'Sin autorización',
      subtitle: 'Sin autorización'
    },
    serverError: {
      path: 'error-servidor',
      fullPath: '/web/error-servidor',
      title: 'Error de servidor',
      subtitle: 'Error de servidor'
    },
    notFound: {
      path: 'pagina-no-encontrada',
      fullPath: '/web/pagina-no-encontrada',
      title: 'Página no encontrada',
      subtitle: 'Página no encontrada'
    }
  };
}
